### !challenge
<!-- type is required, type of challenge -->
<!--'language' is required for type: code-snippet. For python, options are 'python2.7' and 'python3.6' -->
<!-- id is required -->
<!-- title is required -->
* type: code-snippet
* language: python3.6
* id: 4dd64cc8-f0a2-4b7a-916b-709680d6d37e
* title: filter by class
* points: 10
<!-- !question is required -->
### !question
Implement the function `filter_by_class`: It takes a feature matrix, `X`, an array of classes, `y`, and a class label, `label`. It should return all of the rows from X whose label is the given label.
```python
>>> X = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]])
>>> y = np.array(["a", "c", "a", "b"])
>>> filter_by_class(X, y, "a")
array([[1, 2, 3],
       [7, 8, 9]])
>>> filter_by_class(X, y, "b")
array([[10, 11, 12]])
```
### !end-question
<!-- !placeholder is optional, the starter code that will be added to the editor -->
### !placeholder
```python
def filter_by_class(X, y, label):
    '''
    INPUT: 2 dimensional numpy array, numpy array, object
    OUTPUT: 2 dimensional numpy array
    Return the rows from X whose corresponding label from y is the given label.
    '''
    pass
```
### !end-placeholder
 <!-- !tests are required -->
### !tests
```python
import unittest
import main as p
import numpy as np
class TestChallenge(unittest.TestCase):
  def test_filter_by_class1(self):
      x = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]])
      y = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]])
      assert np.array_equal(x, y)
  def test_filter_by_class2(self):
      x = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]])
      y = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]])
      assert np.array_equal(x, y)
  def test_filter_by_class3(self):
      x = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]])
      y = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]])
      assert np.array_equal(x, y)
```
### !end-tests
### !hint
Hay
### !end-hint
### !hint
Use the chat bubble in the lower right corner to ask your instructor for help.
### !end-hint
<!-- !explanation is optional, shown after the student answers correctly -->
### !explanation
```python
def filter_by_class(X, y, label):
    return X[y == label]
```
### !end-explanation
### !end-challenge
